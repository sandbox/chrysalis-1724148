<?php
/**
 * When a select or radio attribute is changed in the UI, the SKUA AJAX callback function is called.
 * The AJAX callback will execute all commands provided by implementations of this hook.
 * @param $form The product add-to-cart form
 * @param $form_state The form_state for the add-to-cart form
 * @return an array of ajax commands
 * @see example taken from uc_nostock module
 */
function hook_uc_skua_callback_alter($form, $form_state) {
  $node = &$form['node']['#value'];
  $parent = '#uc-product-add-to-cart-form-' . $node->nid;
  $commands = array();
  $commands[] = ajax_command_replace($parent . ' .add_to_cart_replace', render($form['actions']['submit']));
  $commands[] = ajax_command_replace($parent . ' .qty_replace', render($form['qty']));
  $commands[] = ajax_command_replace($parent . ' .stock_replace', render($form['stock']));
  return $commands;
}
