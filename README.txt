Copyright Morgan Esch
morgan.esch@gmail.com

SKU Ajax Adjustment (SKUA) is a small utility module that serves two primary purposes:

1.  Finds the SKU of a product based on current attribute selections (select and radio type only).
2.  Provides for hookable AJAX form/page changes when an attribute selection is made via the UI.

The module makes use of the Drupal 7 AJAX API.  No custom javascript is needed.

The only AJAX change made out of the box is to the SKU value (if enabled for display).

Other modules can make further changes by hooking into the AJAX process (see uc_skua.api.php).
