<?php
/**
 *  Implements hook_form_alter()
 */
function uc_skua_form_alter(&$form, &$form_state, $form_id) {
  //Alter product add-to-cart forms
  if (strstr($form_id, 'uc_product_add_to_cart_form_') !== FALSE) {
    $node = &$form['node']['#value'];    
    //Attributes of type 'select' or 'radios' generate product adjustments
    //Collect those attributes and their default options, and add ajax callbacks
    if ($form['attributes'] !== NULL) {
      $combo = array();
      foreach ($form['attributes'] as $att_id => $attribute) {
        if (in_array($attribute['#type'], array('select', 'radios'))) {
          $combo[$att_id] = $form['attributes'][$att_id]['#default_value'];
          //Add AJAX callback
          $form['attributes'][$att_id]['#ajax'] = array('callback' => 'uc_skua_form_callback');
        }
      }
    }

    //Find and store the the product sku.  There are two possibilities:
    //1.  No product adjustments -> the model is the base SKU.
    //2.  Adjustments -> find the SKU based on selected attribute options in two cases:
    //  a.  Initial form build -> if all attributes not required, find the SKU from default options
    //  b.  AJAX -> find SKU if an option has been selected by client for each relevant attribute
    if (!empty($combo)) {
      if (isset($form_state['triggering_element']) &&
        $form_state['triggering_element']['#ajax']['callback'] == 'uc_skua_form_callback') {
        $form_state['temporary']['uc_skua_callback'] = TRUE;
        foreach ($combo as $att_id => $value) {
          $combo[$att_id] = isset($form_state['values']['attributes'][$att_id]) ?
            $form_state['values']['attributes'][$att_id] : '';
        }
      }

      $form_state['storage']['uc_skua_sku'] = uc_skua_find_sku($combo, $node->nid, $node->model);
      $form_state['storage']['uc_skua_combo'] = $combo;
    }
    else {
      $form_state['storage']['uc_skua_sku'] = $node->model;
    }
  }
}

/**
 * Finds the product sku given an attribute-option combination array
 */
function uc_skua_find_sku($combination, $nid, $base_sku) {
  //Ensure an option has been selected for each attribute
  if (!in_array('', $combination)) {
    ksort($combination);
    $model = db_select('uc_product_adjustments', 'a')
      ->fields('a', array('model'))
      ->condition('nid', $nid, '=')
      ->condition('combination', serialize($combination), 'LIKE')
      ->execute()->fetchField();
    //If there is no model for this combination, assume it is using the default SKU
    return $model !== FALSE ? $model : $base_sku;
  }

  return FALSE;
}

/**
 * AJAX callback for relevant attribute changes on add-to-cart form
 */
function uc_skua_form_callback($form, $form_state) {
  $node = &$form['node']['#value'];  
  $sku = $form_state['storage']['uc_skua_sku'] !== FALSE ? $form_state['storage']['uc_skua_sku'] : $node->model;
  $commands = array(ajax_command_replace("#node-$node->nid .model", theme('uc_product_model', array('model' => $sku))));
  $commands = array_merge($commands, module_invoke_all('uc_skua_callback_alter', $form, $form_state));
  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}
